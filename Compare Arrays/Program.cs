﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{

	// Complete the solve function below.
	static int[] solve(int[] a, int[] b)
	{

		int[] _res = new int[2];


		for (int i = 0; i < a.Length; i++) 
		{
			if(a[i] > b[i])
			{
				_res[0]++; // alice
			}
			else if(a[i] < b[i])
			{
				_res[1]++; // bob patiño
			}
		}

		return _res;


	}

	static void Main(string[] args)
	{

		int[] a = new int[3] { 5, 8, 7 };


		int[] b = new int[3] { 3, 6, 10 };

		int[] result = solve(a, b);

		for (int i = 0; i < result.Length; i++)
		{
			Console.Write($"{result[i]} ");

		}

		Console.ReadKey();
	}
}