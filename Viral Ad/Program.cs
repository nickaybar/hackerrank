﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution
{
	// https://www.hackerrank.com/challenges/strange-advertising/problem
    // Solved

	static int Viral(int dias)
	{

		int likes = 0;
		int shared = 0;
		int acumulador = 0;

		shared = 5;
		for (int i = 0; i < dias; i++) // hace 3 vueltas
		{
			if(shared % 2 == 1)
			{
				shared--;
				likes = shared / 2;
				shared = likes * 3;
				acumulador += likes;
			}

			else
			{
				likes = shared / 2;
				shared = likes * 3;
				acumulador += likes;
			}

		}

		return acumulador;

	}

	static void Main(string[] args)
	{

		int n = 5; // 5 dias
		int result = Viral(n);

		Console.WriteLine($"{result}");

		Console.ReadKey();
	}
}