﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sum_Arrays
{
	class Program
	{

		static int SumaArray(int[] vector)
		{
			int _res = 0;

			for(int i= 0; i < vector.Length; i++)
			{
				_res += vector[i];
			}


			return _res ;
		}

		static void Main(string[] args)
		{

			const int ELEMENTOS = 6;
			int[] vec = new int[ELEMENTOS] { 1, 2, 3, 4, 10, 11 };
			int _resultado = 0;

			_resultado = SumaArray(vec);

			Console.WriteLine($"La suma de los elementos del array es {_resultado}");

			Console.ReadKey();
		}
	}
}
